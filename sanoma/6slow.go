// +build OMIT

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/mmcdole/gofeed"
)

func main() {
	categories := []string{
		"Algemeen",
		"Tech",
		"Sport",
		"Entertainment",
	}
	for _, category := range categories {
		fmt.Printf("	%s\n", category)
		articles, err := Get(category)
		if err != nil {
			log.Fatal(err)
		}
		for _, article := range articles {
			fmt.Println(article)
		}
		fmt.Println()
	}
}

type Article struct {
	Title string
	URL   string
}

func (a Article) String() string {
	return fmt.Sprintf("%s - %s", a.URL, a.Title)
}

func Get(category string) ([]Article, error) {
	time.Sleep(1 * time.Second) // HLslow
	resp, err := http.Get("http://www.nu.nl/rss/" + category)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}

	parser := gofeed.NewParser()
	feed, err := parser.Parse(resp.Body)
	if err != nil {
		return nil, err
	}

	articles := make([]Article, len(feed.Items))
	for i, item := range feed.Items {
		articles[i] = Article{
			Title: item.Title,
			URL:   item.GUID,
		}
	}
	return articles, nil
}
