// +build OMIT

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/mmcdole/gofeed"
)

func main() {
	categories := []string{"Algemeen", "Tech", "Sport", "Entertainment"}

	channels := make([]chan []Article, len(categories)) // HLmakeslice
	for i, category := range categories {
		articlesChannel := make(chan []Article)    // HLmakechan
		channels[i] = articlesChannel              // HLset
		go GetByChannel(category, articlesChannel) // HLgo
	}

	for i, category := range categories {
		fmt.Printf("	%s\n", category)
		articlesChannel := channels[i] // HLget
		articles := <-articlesChannel  // HLreceive
		for _, article := range articles {
			fmt.Println(article)
		}
		fmt.Println()
	}
}

type Article struct {
	Title string
	URL   string
}

func (a Article) String() string {
	return fmt.Sprintf("%s - %s", a.URL, a.Title)
}

func Get(category string) ([]Article, error) {
	time.Sleep(1 * time.Second)
	resp, err := http.Get("http://www.nu.nl/rss/" + category)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}

	parser := gofeed.NewParser()
	feed, err := parser.Parse(resp.Body)
	if err != nil {
		return nil, err
	}

	articles := make([]Article, len(feed.Items))
	for i, item := range feed.Items {
		articles[i] = Article{
			Title: item.Title,
			URL:   item.GUID,
		}
	}
	return articles, nil
}

func GetByChannel(category string, articlesChannel chan []Article) { // HLfunc
	articles, err := Get(category) // HLget
	if err != nil {
		log.Fatal(err)
	}
	articlesChannel <- articles // HLsend
}
