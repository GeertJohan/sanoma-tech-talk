// +build OMIT

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/mmcdole/gofeed"
)

func main() {
	categories := []string{ // HLmulti
		"Algemeen",      // HLmulti
		"Tech",          // HLmulti
		"Sport",         // HLmulti
		"Entertainment", // HLmulti
	} // HLmulti
	for _, category := range categories { // HLmulti
		fmt.Printf("	%s\n", category)  // HLmulti
		articles, err := Get(category) // HLmulti
		if err != nil {
			log.Fatal(err)
		}
		for _, article := range articles {
			fmt.Println(article)
		}
		fmt.Println()
	} // HLmulti
}

type Article struct {
	Title string
	URL   string
}

func (a Article) String() string {
	return fmt.Sprintf("%s - %s", a.URL, a.Title)
}

func Get(category string) ([]Article, error) {
	resp, err := http.Get("http://www.nu.nl/rss/" + category)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}

	parser := gofeed.NewParser()
	feed, err := parser.Parse(resp.Body)
	if err != nil {
		return nil, err
	}

	articles := make([]Article, len(feed.Items))
	for i, item := range feed.Items {
		articles[i] = Article{
			Title: item.Title,
			URL:   item.GUID,
		}
	}
	return articles, nil
}
