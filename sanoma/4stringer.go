// +build OMIT

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/mmcdole/gofeed"
)

func main() {
	articles, err := Get("Tech") // HL
	if err != nil {
		log.Fatal(err)
	}
	for _, article := range articles { // HL
		fmt.Println(article)
	}
}

type Article struct { // HLstruct
	Title string // HLstruct
	URL   string // HLstruct
} // HLstruct

func (a Article) String() string {
	return fmt.Sprintf("%s - %s", a.URL, a.Title)
} // HLEND

func Get(category string) ([]Article, error) {
	resp, err := http.Get("http://www.nu.nl/rss/" + category) // HLget
	if err != nil {
		return nil, err // HLreturn
	}
	defer resp.Body.Close()               // HLclose
	if resp.StatusCode != http.StatusOK { // HLstatus
		return nil, errors.New(resp.Status) // HLerrors
	}

	parser := gofeed.NewParser()         // HLparse
	feed, err := parser.Parse(resp.Body) // HLparse
	if err != nil {
		return nil, err // HLreturn
	}

	articles := make([]Article, len(feed.Items)) // HLprepare
	for i, item := range feed.Items {            // HLconvert
		articles[i] = Article{ // HLconvert
			Title: item.Title, // HLconvert
			URL:   item.GUID,  // HLconvert
		} // HLconvert
	}
	return articles, nil // HLreturn
}
