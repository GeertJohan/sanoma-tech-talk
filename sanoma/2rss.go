// +build OMIT

package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/mmcdole/gofeed" // HLimport
)

func main() {
	resp, err := http.Get("http://www.nu.nl/rss/Tech")
	if err != nil {
		log.Fatalf("failed to get RSS feed: %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp.Status)
	}

	parser := gofeed.NewParser()         // HLnewparser
	feed, err := parser.Parse(resp.Body) // HLparse
	if err != nil {                      // HLerror
		log.Fatal(err) // HLerror
	} // HLerror

	for _, item := range feed.Items { // HLprint
		fmt.Println(item.Title) // HLprint
	} // HLprint
}
